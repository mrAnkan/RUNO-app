// RUNO ERASMUS+
// RUNO-App Created by Daniel Holm
// License: Apache License 2.0

angular.module('starter.controllers', [])

  .controller('AppCtrl', function($scope, $state,$ionicModal, $timeout, $ionicHistory) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $scope.goToContent = function(locid, contentid) {
    console.log(locid, +' '+ contentid);
    $ionicHistory.nextViewOptions({
      disableAnimate: true,
      disableBack: true
    });
    $state.go('app.contentDetails', {locationId:locid, contentId: contentid});

  };
  $scope.menu = [
    { title: 'Intro', class: 'about', locationId: 'swe', id: 1 },
    { title: 'Information', class: 'information', locationId: 'swe', id: 8 },
    { title: 'Gallery', class: 'gallery', locationId: 'swe', id: 'gallery' },
    { title: 'Studies Capri & Utö', class: 'title', locationId: 'swe', id: 2 },
    { title: 'Birds', class: 'menuItem', locationId: 'swe', id: 3 },
    { title: 'Flora', class: 'menuItem', locationId: 'swe', id: 4 },
    { title: 'Food & Culture', class: 'menuItem', locationId: 'swe', id: 5 },
    { title: 'Marine Life Utö', class: 'menuItem', locationId: 'swe', id: 6 },
    { title: 'Marine Life Capri', class: 'menuItem', locationId: 'swe', id: 7 },
    { title: 'Studies Monterosa & Kittlefjäll', class: 'title', locationId: 'swe', id: 2 },
    { title: 'Geology History & Climate', class: 'menuItem', locationId: 'swe', id: 8 },
    { title: 'GMO', class: 'menuItem', locationId: 'swe', id: 9 },
    { title: 'Evolution and other factors', class: 'menuItem', locationId: 'swe', id: 10 }
  ];
})

  .controller('contentController',['$scope', '$stateParams', 'ContentList' ,function($scope, $stateParams, ContentList) {
    console.log("$stateParams",$stateParams);

      $scope.content = ContentList.get($stateParams.locationId, $stateParams.contentId);

  }])

  .controller('galleryController',['$scope', '$stateParams', 'ContentList' ,function($scope, $stateParams, ContentList) {
    console.log("$stateParams",$stateParams);

      $scope.content = ContentList.images($stateParams.locationId, $stateParams.contentId);

  }]);
