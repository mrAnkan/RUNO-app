angular.module('starter.services', [])

.factory('ContentList', function() {
  // Might use a resource here that returns a JSON array
  var listContent = [
    {
      date: 'Maj 03, 2016',
      title: 'Birds on Utö and Capri',
      content: 'The purpose of this report is to obtain a deeper understanding of the avifauna on the islands Capri and Utö. The information is gathered through self-made observations of named islands during parts of April and May. Additionally, relevant facts connected to the bird situation on these islands as well as an interview with an ornithologist are important components for the results of this project. The bird migration from subsahara Africa to the brooding areas in northern parts of Europe occurs during the spring season. In Capri and Utö respectively, some bird species are dominant and this project seeks to explain the success of these species. The results of this projects demonstrate that the dominant species on Capri are the herring gull and a number of smaller bird species. In Utö, however, some aquatic bird species are dominant. Common to all the dominant birds is their adaptivity to their surroundings and that the habitats on the islands are similar to the birds’ ecological niches. ',
      images: [{imageUrl: 'img/IMG_1433.JPG', imageText: '' , fontColor: "#fff"}, {imageUrl: 'img/bird.jpg', imageText: '', fontColor: "#fff"}],
      locationId: 'swe',
      participants: 'Leonard, Marcus & Mattias',
      id: 3
    },
    {
      date: 'Maj 03, 2016',
      title: 'Flora on Utö and Capri',
      content: 'What could the Mediterranean scrubland have in common with the boreal forest? Are they more different than they are similar? And what could be the cause of this? This study compares islands Utö and Capri, one located in the Baltic Sea and the other in the Mediterranean. Focusing on the species found on these two islands, along with their prefered habitat, this report analyses and answers these three critical questions, by exploring both Utö and capri through field studies. We found that the difference in soil, pH-value and other abiotic factors have resulted in the many differences but also few similarities between these two small islands. The different types of vegetation, such as the mediterranean bush and the arboreal euphorbia mainly consist of plants that prefer drier and poorer soil, which is why Capri primarily is inhabited by shrub plants. The one exception is the ilex woods, where the soil is richer in moisture and nutrients. Utö only consists of one type of vegetation: boreal forest. Still there are similar species in Utö as in Capri. ',
      locationId: 'swe',
      images: [{imageUrl: 'img/IMG_1390.JPG', imageText: '' , fontColor: "#fff"}, {imageUrl: 'img/IMG_1167.JPG', imageText: '', fontColor: "#fff"}],
      participants: 'Jowana & Mona',
      id: 4 },
      {
        date: 'Maj 03, 2016',
        title: 'Food & Culture on Utö',
        content: 'Most islands in the world are closely connected to both fishing cultures, boating and tourism. The purpose of our case was to investigate to which degree those aspects affect the food culture. Production, distribution and consumption are key factors, both on Utö in the Swedish archipelago and on the Italian isle Capri. Focus was put on gathering as much information as possible, mainly from restaurant owners and locals, to see exactly how the chosen aspects are connected to the food culture. It appears that islanders make their money during the summer season, as a result of tourism. Visitors want to try local dishes, and the seas surrounding both islands contribute to the idea that fish is one of them. On Capri, the fish is caught nearby, while on Utö an established fishing industry never really existed. By other means, what the tourists perceive as local food on Utö has little connection to the actual food culture. The islands differ tremendously in terms of population, which is an adequate explanation as to why important differences exist.',
        images: [{imageUrl: 'img/IMG_1129.JPG', imageText: '' , fontColor: "#fff"}, {imageUrl: 'img/IMG_3887.JPG', imageText: '', fontColor: "#fff"}],
        locationId: 'swe',
        participants: 'Wilma & Alice',
        id: 5 },
        {
          date: 'Maj 03, 2016',
          title: 'Marine Life on island Utö',
          content: 'The field study on Utö lasted three days. Our main objective was to gather water samples as we had done previously on Capri. The whole group of Erasmus+ students rented bikes and travelled a long way to the far side of a connecting island, Ålö. Along the way water samples were gathered, however because of the temperature we chose to collect samples from the surface water only and not from the depth of one meter. Temperature and pH value was also read. We were also allowed to borrow literature on marine life in the Baltic sea from the local school on Utö. Once again we were reunited with a few of the italian students we had become friends with on Capri, but we also met new Italian students who became part of the group quickly.',
          images: [{imageUrl: 'img/water2.jpg', imageText: '', fontColor: "#fff"}],
          locationId: 'swe',
          participants: 'Ida, Emil & Linn',
          id: 6
        },
        {
          date: 'Maj 03, 2016',
          title: 'Marine Life on Capri',
          content: 'The field study on Capri spanned five days, during which time we gathered quite a lot of useful information. Water samples were collected on three different locations on the the island. Temperature and pH value was also read. The aim was to gather two samples from every location, one of the surface water and one on the depth of one meter. But because of the weather conditions we felt at times that our safety in the water could not be guaranteed and so we only collected the sample of the surface water. We also went on a trek along the Sentiero dei Fortini where information plaques along the way have us information about the marine life surrounding Capri. We also met with the Italians for the first time and had a wonderful time with them on Capri.',
          images: [{imageUrl: 'img/water1.jpg', imageText: '' , fontColor: "#fff"}],
          locationId: 'swe',
          participants: 'Ida, Emil & Linn',
          id: 7
        },
        {
          date: 'Maj 03, 2016',
          title: 'Geology History & Climate',
          content: "About 100 million years ago, the African and Arabian tectonical plates collided with the Euroasian tectonical plate, during the movement of Pangea. This created a folding of mountains. The Apls are one of these mountain groups and they are still rising, since the convergence is still active. The Alps reaches up to 4000 metres above sea level and they mainly consist of gneiss and granit. Limestone formed the seabed beneath the Alps, and therefore, the rocks of the Apls also include a high level of limestone.   The Baltic plate got their mountains by volcanism and sedimentation, approximately around 400 million years ago, within the time span silur to devon. Vulcans were made in the crack of the Atlantic ocean plate, because of the active divergence back then. Under the moves of Pangea, the north American plate Laurentia collided into the north European plate Baltica, pushing forward these volcanos up to Baltica. This motion created huge mountains, but over periods when the convergence retired, they were eroded and grew smaller. The result was a firmly high and milled mountain chain, that we today call the Scands, located in Sweden and Norway. The approximate height of the Scands are 2000 metres above sea level.  Ice ages over the years were a big effecting factor on today's climate of the Apls and the Scands. Under these ice ages, moving glaciers pushed down and erodated the valleys of the mountains. When the glacier melted, lakes were created. Below the mountains in the valleys the temperature is warmer, because the heat is surrounded by the mountains. Focusing on the Alps climate, warm winds blowing from the equator cools off at the top of the mountains and precipitates in form of rain or snow. This gives a various climate, both good and bad for the agriculture. ",
          images: [{imageUrl: 'img/GeologyHistoryAndClimate.jpg', imageText: ''}],
          locationId: 'swe',
          participants: '',
          id: 8
        },
        {
          date: 'Maj 03, 2016',
          title: 'GMO',
          content: 'GMOs, genetically modified organisms, involves adding or enhancing genes for improving the properties of organisms. For example Valseserna or Sami in different ways to use GMO cows or reindeer for example producing lactose-free milk or develop greater body mass.In both Kittelfjäll and Alagna, one could take advantage of GMO in agriculture. However, Italy has firmly stated that they are against GMOs in the production, sale and import. In Sweden we have not really taken a position on the issue yet.The reason that so many countries are against the cultivation of GMO is that there are great risks with it. If GMO for example spread to surrounding plants and eventually have a widespread and out-compete other plants. If adding a gene that confers resistance to a particular viral gene can spread to some plants, which then has an advantage and can grow larger and stronger, and thus outcompete other plants that lack this gene. This in turn can lead to the emergence of resistance gene takes the ousted plant site and spread itself, which can be a threat to biodiversity. When biodiversity loss also reduces the supply of many herbivores that also has negative consequences for the rest of the ecosystem.',
            images: [{imageUrl: 'img/reindeer-79082.jpg', imageText: '' , fontColor: "#fff"}, {imageUrl: 'img/south-tyrol-230239.jpg', imageText: '', fontColor: "#fff"}],
          locationId: 'swe',
          participants: 'Pupils Biology 1',
          id: 9
        },
        {
          date: 'Maj 03, 2016',
          title: 'Evolution and other factors',
          content: "When there’s less trees, the biotic factors won’t be as tough.  By making it possible for flowers to spread and increase the genetic variation and the biodiversity, we will speed up the evolution. More generations in less time, the possibilities to random beneficial mutations increases and more species have the chance to change and develop, which finally leads to the evolution progressing faster. Most of the mutations are damaging, however there are also beneficial ones. The good thing is that the bad mutations have it more difficult to survive in comparison to the good ones.  A mutation can occur when the protein sequence in an organism changes. This happens right before the organism copies its DNA to get the right amount of chromosomes before it divides itself (replication). They can be good, bad, or have no function at all for the organism. If the mutation occurs in a gamete (meiosis) the daughter cell that's created gets a change in its DNA that the mother cell doesn’t have. That specific mutation can then be passed on to the next generation and then to the next and so on (genetic variation), which at the end leads to the biodiversity increasing as well. The mutations can however not be passed on if they occur in an autosomal cell (mitosis). By that we can say that mutations in gametes passes on to the upcoming generations and helps the evolutional process develop.",
          images: [{imageUrl: 'img/ears-1452991.jpg', imageText: ''},{imageUrl: 'img/tomato-61738.jpg', imageText: ''}],
          locationId: 'swe',
          participants: 'Pupils Biology 1',
          id: 10
        },
        {
          date: 'Maj 03, 2016',
          title: 'Image Gallery Capri',
          content: '',
          images: [
            {imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIVHp1Q3BUZWxLY1U', imageText: ''}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIMTVmcU1nTklCOFE', imageText: 'Villa San Michele'}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B1MoswWmGrZ0eGtMRmliYUUxZUU', imageText: 'Anacapri Mayor\'s Office'}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIbE9NcWJHbDhSR1k', imageText: ''}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIMjIxOU5YdVlUS00', imageText: ''}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIdlRoSjRHRWV0UVk', imageText: ''}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIbVpkZDByN3l1MVE', imageText: ''}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIdTN6NEtySjhQa0E', imageText: 'Grotta Azzura'}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIcF9JUVI4UTZMMlk', imageText: 'Limestones of Capri'}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIa0ZmZHhjWmoyemc', imageText: ''}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsINVNJZjVMd3Q4azQ', imageText: ''}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIZUNCbS1RaGRsNkE', imageText: 'Fiat'}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIS0JEVlRxQjFSV2c', imageText: ''}
            ,{imageUrl: 'https://drive.google.com/uc?export=download&id=0B3KNdZBhjbsIdm5RMW9ISm5IcWs', imageText: 'Axel Munthe'}
        ],
          locationId: 'swe',
          participants: '',
          id: 44
        }
      ];

      return {
        all: function() {
          return listContent;
        },
        get: function(locationId, contentId) {
          console.log(locationId, contentId);
          var result = $.grep(listContent, function(e){
            return e.locationId == locationId;
          });
          console.log(result);

          for (var i = 0; i < result.length; i++) {
            console.log(i);
            if (result[i].id === parseInt(contentId)) {
              console.log("ja");
              console.log(result[i]);
              return result[i];
            }
          }
          return null;
        },
        images: function(locationId, contentId) {
          console.log(locationId, contentId);
          var result = $.grep(listContent, function(e){
            return e.locationId == locationId;
          });
          console.log(result);
          var arr = [];
          for (var i = 0; i < 46; i++) {
            console.log(i);
            if (result[i].only_image) {
              console.log("ja");
              console.log(result[i]);
              arr.push(result[i]);
              console.log(arr);
              return arr;
            }
          }
          return null;
        }
      };
    })
