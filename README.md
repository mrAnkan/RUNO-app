# RUNO-app
Application for Rudbeck Erasmus+

### Erasmus+, European Cooperation in Education
### "The Great Beauty"
### Exchange of good practices for a better European school.

### The Purpose of RUNO-app:
The RUNO-app can be used for field work in the  natural area of Monte Rosa in Italy, Kittelfjäll and Utö in Sweden.

Visit http://rudbeck.se for information 